package mt.forkjoin;

import java.util.Random;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

public class ForkJoinMax extends RecursiveTask<Integer> {

	private static final int N_CORES = Runtime.getRuntime().availableProcessors();
	
	private static final int N_THREADS = N_CORES;

	private static final int N_ITEMS = 100 * 1000 * 1000;

	private static final int SEQUENTIAL_THRESHOLD = N_ITEMS / N_CORES;

	private final int[] data;
	private final int start;
	private final int end;

	public ForkJoinMax(int[] data, int start, int end) {
		this.data = data;
		this.start = start;
		this.end = end;
	}

	public ForkJoinMax(int[] data) {
		this(data, 0, data.length);
	}

	@Override
	protected Integer compute() {
		final int length = end - start;
		if (length <= SEQUENTIAL_THRESHOLD) {
			return computeDirectly();
		}
		final int split = length / 2;
		final ForkJoinMax left = new ForkJoinMax(data, start, start + split);
		left.fork();
		final ForkJoinMax right = new ForkJoinMax(data, start + split, end);
		return Math.max(right.compute(), left.join());
	}

	private Integer computeDirectly() {
//		System.out.println(Thread.currentThread() + " computing: " + start
//				+ " to " + end);
		int max = Integer.MIN_VALUE;
		for (int i = start; i < end; i++) {
			if (data[i] > max) {
				max = data[i];
			}
		}
		return max;
	}

	public static void main(String[] args) {
		final int[] data = createRandomData();
		for (int i = 0; i < 100; i++) {
			long start = System.nanoTime();
			findMaxForkJoin(data);
			System.out.println("Fork-Join took " + (System.nanoTime() - start)
					/ 1000000 + "ms");
		}
	}

	private static void findMaxForkJoin(final int[] data) {
		final ForkJoinPool pool = new ForkJoinPool(N_THREADS);
		final ForkJoinMax finder = new ForkJoinMax(data);
		System.out.println(pool.invoke(finder));
	}

	private static int[] createRandomData() {
		final int[] data = new int[N_ITEMS];
		final Random random = new Random();
		for (int i = 0; i < data.length; i++) {
			data[i] = random.nextInt(100);
		}
		return data;
	}
}